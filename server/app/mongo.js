const express = require('express')
const app = express()
const mongoose = require('mongoose')
const keys = require('./keys')

mongoose
  .connect(`${keys.MONGO_URI}/${keys.APP_NAME}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log(
    `MongoDB connected - ${keys.MONGO_URI}/${keys.APP_NAME}`),
  )
  .catch(e => console.error(e))

module.exports = {app}
