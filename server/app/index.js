const {app, server} = require('./mongo')
const bodyParser = require('body-parser')

const pollsRoutes = require('../collections/polls/route')

app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

app.use('/api/polls', pollsRoutes)

module.exports = {app, server}
