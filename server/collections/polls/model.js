const {model, Schema} = require('mongoose')

const pollSchema = new Schema({
  createDate: {
    type: Date,
    default: new Date(),
  },
  conditions: [],
})

module.exports = model('polls', pollSchema)
