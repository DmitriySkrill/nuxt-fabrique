const polls = require('./model')

module.exports.create = async (req, res) => {
  try {
    res.status(200).json(await (new polls(req.body).save()))
  } catch (e) {
    res.status(500).json(e)
  }
}
module.exports.get = async (req, res) => {
  try {
    const
      filter = typeof req.query.filter === 'string'
        ? JSON.parse(req.query.filter)
        : req.query.filter

    res.status(200).json(await polls.find(filter))
  } catch (e) {
    res.status(500).json(e)
  }
}
module.exports.remove = async (req, res) => {
  try {
    let _id = req.body._id
    let result = await polls.deleteOne({_id})
     let response = {}
    if (result.ok) {
      response._id = _id
      response.result = result
      response.message = 'Сотрудник удален'
      response.error = false
    } else {
      response.message = `Ошибка при удалении сотрудника с _id: ${_id}.`
      response.error = true
    }
    res.json(response)
  } catch (error) {
    res.status(500).json(error)
  }
}
