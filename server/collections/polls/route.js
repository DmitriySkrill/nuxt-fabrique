const {Router} = require('express')
const {get, create, remove} = require('./controller')
const router = Router()

router.post('', create)
router.get('', get)
router.delete('', remove)

module.exports = router
