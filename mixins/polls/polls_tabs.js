export default {
  computed:{
    tabs() {
      return this.$store.state.collections.polls_store.tabs
    },
    activeTab(){
      return this.tabs.find(tab => tab.active)
    }
  }
}
