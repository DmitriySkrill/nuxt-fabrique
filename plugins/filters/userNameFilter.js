module.exports = {
  fullUserName: (user) => {
    if (!user) {
      return
    }
    let text = ''
    text += user.surname ? `${user.surname} ` : ''
    text += user.name ? `${user.name} ` : ''
    text += user.patronymic ? `${user.patronymic}` : ''
    return text
  },
  shortUserName: (user) => {
    if (!user) {
      return
    }
    let text = ''
    text += user.surname ? `${user.surname} ` : ''
    text += user.name ? `${user.name[0]}.` : ''
    text += user.patronymic ? `${user.patronymic[0]}.` : ''
    return text
  }
}
