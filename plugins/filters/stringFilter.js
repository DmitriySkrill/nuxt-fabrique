module.exports = {
  capitalizeFirstLetter: (string) => {
    if (!string) return ''
    if (string.length === 0) return 0
      return string[0].toUpperCase() + string.slice(1)
  },
}
