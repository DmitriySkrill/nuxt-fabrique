module.exports = {
  date (value) {
    if (!value) {return ''}
    const options = {
      year: '2-digit',
      month: '2-digit',
      day: '2-digit',
    }
    return new Date(value).toLocaleDateString('ru-RU', options)
  },
  time (value) {
    if (!value) {return ''}
    const options = {
      hour: '2-digit',
      minute: '2-digit'
    }
    return new Date(value).toLocaleTimeString('ru-RU', options)
  },
  dateTime (value) {
    // if (!value) {return ''}
    const options = {
      year: '2-digit',
      month: '2-digit',
      day: '2-digit',
      hour: '2-digit',
      minute: '2-digit'
    }
    return new Date(value).toLocaleDateString('ru-RU', options)
  }
}
