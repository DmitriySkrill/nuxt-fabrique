import Vue from 'vue'

import {fullUserName, shortUserName} from './userNameFilter'
import {date, time, dateTime} from './dateFilter'
import {capitalizeFirstLetter} from './stringFilter'

Vue.filter('fullUserName', fullUserName)
Vue.filter('shortUserName', shortUserName)
Vue.filter('date', date)
Vue.filter('time', time)
Vue.filter('dateTime', dateTime)
Vue.filter('capitalizeFirstLetter', capitalizeFirstLetter)

