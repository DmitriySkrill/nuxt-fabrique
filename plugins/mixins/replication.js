export default {
  methods: {
    copyWithoutLinks(data) {
      if (!data) {
        return null
      }
      return JSON.parse(JSON.stringify(
        data
      ))
    },
  },
}
