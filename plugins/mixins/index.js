import Vue from 'vue'
import replication from '@/plugins/mixins/replication'

Vue.mixin({
  methods: {
    ...replication.methods,
  },

})
