export const state = () => ({
  polls: [],
  pollRespondentConditions: [
    {
      _id: '111',
      codeId: 'respondentAge',
      title: 'Возраст респондента',
      conditionTypeTitle: 'диапазон',
    },
    {
      _id: '222',
      codeId: 'loyaltyCardType',
      title: 'Тип карты лояльности',
      conditionTypeTitle: 'тип',
      typeList: [
        {_id: '2221', codeId: 'gold', title: 'Gold'},
        {_id: '2222', codeId: 'platinum', title: 'Platinum'},
      ],
    },
    {
      _id: '333',
      codeId: 'loyaltyCardStatus',
      title: 'Статус карты лояльности',
      conditionTypeTitle: 'статус',
      typeList: [
        {_id: '3331', codeId: 'active', title: 'Активна'},
        {_id: '3332', codeId: 'inactive', title: 'Неактивна'},
        {_id: '3333', codeId: 'unknown', title: 'Неизвестно'},
      ],
    },
  ],
})

export const mutations = {
  set(state, {endpoint, elements = []}) {
    state[endpoint] = elements
  },
  add(state, {endpoint, element}) {
    if (!element) return false
    if (!element._id) return false

    let index = state[endpoint].find(oldEl =>
      element._id === oldEl._id,
    )
    if (index >= 0) {
      state[endpoint].splice(index, 1, element)
    } else {
      state[endpoint].push(element)
    }

  },
  delete(state, {endpoint, _id}) {
    let index = state[endpoint].findIndex(el => el._id === _id)
    if (index >= 0) {
      state[endpoint].splice(index, 1)
    }
  },
}

export const actions = {
  async get({commit}, {endpoint, filter}) {
    try {
      let elements = await this.$axios.$get(`/api/${endpoint}`, {
        params: {filter},
      })
      commit('set', {endpoint, elements})
    } catch (e) {
      console.error(e)
    }
  },
  async post({commit}, {endpoint, element}) {
    try {
      let newElement = await this.$axios.$post(`/api/${endpoint}`, element)
      commit('add', {endpoint, element: newElement})
    } catch (e) {
      console.error(e)
    }
  },
  async remove({commit, dispatch}, {_id, endpoint}) {
    try {
      const result = await this.$axios.$delete(
        `/api/${endpoint}`,
        {data: {_id}},
      )
      commit('delete', {_id: result._id, endpoint})
    } catch (e) {
      console.error(e)
    }
  },
}

export const getters = {}
