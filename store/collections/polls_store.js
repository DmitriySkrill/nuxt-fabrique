export const state = () => ({
  headers: [
    {width: '50', key: '_id', title: '_id'},
    {width: '20', key: 'createDate', title: 'Дата создания'},
    {width: '10', key: 'conditions', title: 'Условия'},
    {width: '20', key: 'actions', title: 'Действия'},
  ],
  tabs: [
    {key: 'parameters', title: 'Параметры', active: false},
    {key: 'questions', title: 'Вопросы', active: false},
    {key: 'logic', title: 'Логика', active: false},
    {key: 'conditions', title: 'Условия', active: false},
    {key: 'respondents', title: 'Респонденты', active: true},
  ],
  pollRespondentConditions: [],
  colors: [
    {
      color: 'rgb(128,0,0)',
      backgroundColor: 'rgb(128,0,0,0.1)',
      typeBackgroundColor: 'rgb(128,0,0,0.3)',
    },
    {
      color: 'rgb(0,0,128)',
      backgroundColor: 'rgb(0,0,128,0.1)',
      typeBackgroundColor: 'rgb(0,0,128,0.3)',
    },
    {
      color: 'rgb(0,128,0)',
      backgroundColor: 'rgb(0,128,0,0.1)',
      typeBackgroundColor: 'rgb(0,128,0,0.3)',
    },
    {
      color: 'rgb(128,128,0)',
      backgroundColor: 'rgb(128,128,0,0.1)',
      typeBackgroundColor: 'rgb(128,128,0,0.3)',
    },
    {
      color: 'rgb(128,0,128)',
      backgroundColor: 'rgb(128,0,128,0.1)',
      typeBackgroundColor: 'rgb(128,0,128,0.3)',
    },
  ],
})

function getColorIndex(conditionCount, colorCount) {
  if (typeof conditionCount !== 'number'
    || typeof colorCount !== 'number'
  ) return -1
  if (isNaN(conditionCount) || isNaN(colorCount)) return -1
  let multiplicity = parseInt(conditionCount / colorCount)
  return conditionCount - multiplicity * colorCount
}

export const mutations = {
  setState(state, {endpoint, data}) {
    state[endpoint] = data
  },
  addPollRespondentCondition(state, newCondition = {}) {
    const colorIndex = getColorIndex(
      state.pollRespondentConditions.length, state.colors.length,
    )
    let {color, backgroundColor, typeBackgroundColor} = state.colors[colorIndex]
    state.pollRespondentConditions.push(
      {...newCondition, color, backgroundColor, typeBackgroundColor})
  },
  updatePollRespondentCondition(state, {
    index, pollRespondentConditionToUpdate = {},
  }) {
    state.pollRespondentConditions.splice(
      index, 1, pollRespondentConditionToUpdate,
    )
  },
  deletePollRespondentCondition(state, payload = {}) {
    let index
    if (!payload.index && payload.index !== 0) {
      index = state.pollRespondentConditions.length - 1
    } else {
      index = payload.index
    }
    state.pollRespondentConditions.splice(index, 1)
  },
}
