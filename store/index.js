export const state = () => ({
  user: {
    name: 'Алексей',
    surname: 'Иванов',
    patronymic: 'Петрович',
    workPosition: 'Администратор',
    photoURL: '/upload/userPhoto.jpg',
  },
})

export const mutations = {}

export const actions = {}

export const getters = {}
