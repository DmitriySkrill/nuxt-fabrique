export const state = () => ({
  pages: [
    {icon: 'home', title: 'Главная', to: '/', showInMenus: ['nav']},
    {icon: 'description', title: 'Опросы', to: '/polls', showInMenus: ['nav']},
    {
      icon: '',
      title: 'Добавить опрос',
      to: '/polls/add',
      showInMenus: [],
    },
    {
      icon: 'supervisor_account',
      title: 'Пользователи',
      to: '/users',
      showInMenus: ['nav'],
    },
    {
      icon: 'flag',
      title: 'Черные списки',
      to: '/blackLists',
      showInMenus: ['nav'],
    },
    {
      icon: 'call',
      title: 'Колл-центр',
      to: '/callCenter',
      showInMenus: ['nav'],
    },
    {
      icon: 'help',
      title: '',
      to: '/help',
      showInMenus: ['nav'],
    },
  ],
})

export const getters = {
  navMenu: state => state.pages.filter(p => p.showInMenus.includes('nav')),
}
